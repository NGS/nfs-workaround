
//#define DEBUG

#define _GNU_SOURCE
#ifdef DEBUG
#  include <stdio.h>
#  define LOG_PREFIX "NFS workaround: "
#endif
#include <dlfcn.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>

static int (*libc_open)(const char *name, int oflag, ...) = NULL;
static int (*libc_open64)(const char *name, int oflag, ...) = NULL;
static int (*libc_openat)(int dirfd, const char *name, int oflag, ...) = NULL;
static int (*libc_openat64)(int dirfd, const char *name, int oflag, ...) = NULL;
static int (*libc_creat)(const char *name, mode_t mode) = NULL;
static int (*libc_creat64)(const char *name, mode_t mode) = NULL;
static int (*libc_fsync)(int fd) = NULL;
void bad_constants();

const mode_t required_mode[3] = {
	[O_RDONLY] = S_IRUSR,
	[O_RDWR]   = S_IRUSR|S_IWUSR,
	[O_WRONLY] = S_IWUSR,
};

int open(const char *name, int oflag, ...) {
	if (! (oflag & O_CREAT)) {
		return libc_open(name, oflag);
	}
	mode_t mode;
	{
		va_list args;
		va_start(args, oflag);
		mode = va_arg(args, mode_t);
		va_end(args);
	}
	int req_mode = required_mode[oflag & 3];
	if ((mode & req_mode) != req_mode) {
		if (access(name, F_OK) != -1) {
			return libc_open(name, oflag, mode);
		}
		mode_t fixed_mode = mode | req_mode;
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "open(%s, %x, %o -> %o)\n", name, oflag, mode, fixed_mode);
#endif
		int fd=libc_open(name, oflag, fixed_mode);
		if (fd != -1) {
			mode_t um = umask(0);
			umask(um);
			int ret=chmod(name, mode & ~um);
			if (ret == -1) {
				close(fd);
				return -1;
			}
		}
		return fd;
	}
	return libc_open(name, oflag, mode);
}

int open64(const char *name, int oflag, ...) {
	if (! (oflag & O_CREAT)) {
		return libc_open64(name, oflag);
	}
	mode_t mode;
	{
		va_list args;
		va_start(args, oflag);
		mode = va_arg(args, mode_t);
		va_end(args);
	}
	int req_mode = required_mode[oflag & 3];
	if ((mode & req_mode) != req_mode) {
		if (access(name, F_OK) != -1) {
			return libc_open64(name, oflag, mode);
		}
		mode_t fixed_mode = mode | req_mode;
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "open64(%s, %x, %o -> %o)\n", name, oflag, mode, fixed_mode);
#endif
		int fd=libc_open64(name, oflag, fixed_mode);
		if (fd != -1) {
			mode_t um = umask(0);
			umask(um);
			int ret=chmod(name, mode & ~um);
			if (ret == -1) {
				close(fd);
				return -1;
			}
		}
		return fd;
	}
	return libc_open64(name, oflag, mode);
}

int openat(int dirfd, const char *name, int oflag, ...) {
	if (! (oflag & O_CREAT)) {
		return libc_openat(dirfd, name, oflag);
	}
	mode_t mode;
	{
		va_list args;
		va_start(args, oflag);
		mode = va_arg(args, mode_t);
		va_end(args);
	}
	int req_mode = required_mode[oflag & 3];
	if ((mode & req_mode) != req_mode) {
		if (access(name, F_OK) != -1) {
			return libc_openat(dirfd, name, oflag, mode);
		}
		mode_t fixed_mode = mode | req_mode;
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "openat(%s, %x, %o -> %o)\n", name, oflag, mode, fixed_mode);
#endif
		int fd=libc_openat(dirfd, name, oflag, fixed_mode);
		if (fd != -1) {
			mode_t um = umask(0);
			umask(um);
			int ret=chmod(name, mode & ~um);
			if (ret == -1) {
				close(fd);
				return -1;
			}
		}
		return fd;
	}
	return libc_openat(dirfd, name, oflag, mode);
}

int openat64(int dirfd, const char *name, int oflag, ...) {
	if (! (oflag & O_CREAT)) {
		return libc_openat64(dirfd, name, oflag);
	}
	mode_t mode;
	{
		va_list args;
		va_start(args, oflag);
		mode = va_arg(args, mode_t);
		va_end(args);
	}
	int req_mode = required_mode[oflag & 3];
	if ((mode & req_mode) != req_mode) {
		if (access(name, F_OK) != -1) {
			return libc_openat64(dirfd, name, oflag, mode);
		}
		mode_t fixed_mode = mode | req_mode;
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "openat64(%s, %x, %o -> %o)\n", name, oflag, mode, fixed_mode);
#endif
		int fd=libc_openat64(dirfd, name, oflag, fixed_mode);
		if (fd != -1) {
			mode_t um = umask(0);
			umask(um);
			int ret=chmod(name, mode & ~um);
			if (ret == -1) {
				close(fd);
				return -1;
			}
		}
		return fd;
	}
	return libc_openat64(dirfd, name, oflag, mode);
}

int creat(const char *name, mode_t mode) {
	if (~mode & S_IWUSR) {
		if (access(name, F_OK) != -1) {
			return libc_creat(name, mode);
		}
		mode_t fixed_mode = mode | S_IWUSR;
		int fd=libc_creat(name, fixed_mode);
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "creat(%s, %o -> %o)\n", name, mode, fixed_mode);
#endif
		if (fd != -1) {
			mode_t um = umask(0);
			umask(um);
			int ret=chmod(name, mode & ~um);
			if (ret == -1) {
				close(fd);
				return -1;
			}
		}
		return fd;
	}
	return libc_creat(name, mode);
}

int creat64(const char *name, mode_t mode) {
	if (~mode & S_IWUSR) {
		if (access(name, F_OK) != -1) {
			return libc_creat64(name, mode);
		}
		mode_t fixed_mode = mode | S_IWUSR;
		int fd=libc_creat64(name, fixed_mode);
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "creat64(%s, %o -> %o)\n", name, mode, fixed_mode);
#endif
		if (fd != -1) {
			mode_t um = umask(0);
			umask(um);
			int ret=chmod(name, mode & ~um);
			if (ret == -1) {
				close(fd);
				return -1;
			}
		}
		return fd;
	}
	return libc_creat64(name, mode);
}

int fsync(int fd) {
	int olderrno=errno;
	int ret = libc_fsync(fd);
	if (ret == -1 && errno == EACCES) {
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "fsync(%d) EACCES, skipping it and restoring errno=%d\n", fd, olderrno);
#endif
		errno=olderrno;
		return 0;
	}
	return ret;
}

void __attribute((constructor)) init_nfs_workaround () {
	if (O_RDWR != 2 || O_WRONLY != 1 || O_RDONLY != 0) {
#ifdef DEBUG
		fprintf(stderr, LOG_PREFIX "Bad constants : %i, %i, %i (should be 0, 1, 2)\n", O_RDONLY, O_WRONLY, O_RDWR);
#endif
		bad_constants();
	}
	libc_open=dlsym(RTLD_NEXT, "open");
	libc_open64=dlsym(RTLD_NEXT, "open64");
	libc_openat=dlsym(RTLD_NEXT, "openat");
	libc_openat64=dlsym(RTLD_NEXT, "openat64");
	libc_creat=dlsym(RTLD_NEXT, "creat");
	libc_creat64=dlsym(RTLD_NEXT, "creat64");
	libc_fsync=dlsym(RTLD_NEXT, "fsync");
#ifdef DEBUG
	for (unsigned i=0; i<3; i++) {
		fprintf(stderr, LOG_PREFIX "required mode for access %i: %o\n", i, required_mode[i]);
	}
	fprintf(stderr, LOG_PREFIX "open: %p, libc open: %p\n", open, libc_open);
	fprintf(stderr, LOG_PREFIX "open64: %p, libc open64: %p\n", open64, libc_open64);
	fprintf(stderr, LOG_PREFIX "openat: %p, libc openat: %p\n", openat, libc_openat);
	fprintf(stderr, LOG_PREFIX "openat64: %p, libc openat64: %p\n", openat64, libc_openat64);
	fprintf(stderr, LOG_PREFIX "creat: %p, libc creat: %p\n", creat, libc_creat);
	fprintf(stderr, LOG_PREFIX "creat64: %p, libc creat64: %p\n", creat64, libc_creat64);
	fprintf(stderr, LOG_PREFIX "fsync: %p, libc fsync: %p\n", fsync, libc_fsync);
#endif
}
