

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

int main() {
	{
		unlink("test1");	
		FILE* file=fopen("test1", "w+");

		if (!file) {
			perror("test1");
		} else {
			printf("test1: OK\n");
			fclose(file);
		}
	}

	{
		unlink("test2");	
		int fd=open("test2", O_RDWR|O_CREAT|O_EXCL|O_TRUNC, 0444);
		if (fd == -1) {
			perror("test2");
		} else {
			write(fd, "coucou\n", 7);
			printf("test2: OK\n");
			close(fd);
		}
	}

	return 0;
}

